Format: 3.0 (quilt)
Source: node-figures
Binary: node-figures
Architecture: all
Version: 2.0.0-1
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Vysakh Dileep <vysakhdileep@gmail.com>
Homepage: https://github.com/sindresorhus/figures#readme
Standards-Version: 4.1.3
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-javascript/node-figures.git
Vcs-Git: https://anonscm.debian.org/git/pkg-javascript/node-figures.git
Testsuite: autopkgtest
Build-Depends: debhelper (>= 9), dh-buildinfo, nodejs
Package-List:
 node-figures deb javascript optional arch=all
Checksums-Sha1:
 b0678acf49093ad3302a074c8f19ec4dba65326c 9842 node-figures_2.0.0.orig.tar.gz
 e57dfba9c51672c4fc5e5f48f55631577e43dbc2 2088 node-figures_2.0.0-1.debian.tar.xz
Checksums-Sha256:
 bc7f904a926282de0f30594485a4e5e65463bd3bbd6ea8052a5a80fee8b13daa 9842 node-figures_2.0.0.orig.tar.gz
 54e77570cefc78527fb9907b731ca054cc0116905da2192d13737d0b2f741a86 2088 node-figures_2.0.0-1.debian.tar.xz
Files:
 c68ae3241eb8e9251f51e76dddbc1a87 9842 node-figures_2.0.0.orig.tar.gz
 92e1c07889b9a6399718e1e3870760f9 2088 node-figures_2.0.0-1.debian.tar.xz
